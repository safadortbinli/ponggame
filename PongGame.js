(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'

        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left:0,
            top: 150
        },
        stick2: {
          right:0,
          top:150
        },
        add:{
          position: 'relative',
          top:50,
          left:200,
        },
        add1:{
          position: 'relative',
          top:20,
          left:650,
          
        }
       
    };

    var CONSTS = {
    	gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0,
        puan1:0,
        puan2:0,  
    };

    var stop = true;
    function start() {
        if(!stop){return}
        draw();
        setEvents(); // key kontrol
        setEvents_2(); 
        roll();		// topun başlangıç ve çıkış yeri
        loop();    // topun hareketleri döngü
    }

    function draw() {
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<p/>',   {id: 'pn1'}).css(CSS.add).appendTo('#pong-game').text(0);
        $('<p/>',   {id: 'pn2'}).css(CSS.add1).appendTo('#pong-game').text(0);
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick)).appendTo('#pong-game');
    }

    function setEvents() {           // soldaki stick klavye kontrol 
            if(!stop){return}
        $(document).on('keydown', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = -5;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = 0;
            }            
        });

        $(document).on('keydown', function (e) {
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = 5;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = 0;
            }            
        });
      }

    function setEvents_2() {     // sağdaki stick klavye kontrol 
            if(!stop){return}
               $(document).on('keydown', function (e) {
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = -5;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = 0;
            }
        });
        $(document).on('keydown', function (e) {
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = 5;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = 0;
            }
        });
    }

    function loop() {
        window.pongLoop = setInterval(function () {
          if(!stop){return}
            CSS.stick1.top += CONSTS.stick1Speed;
            $('#stick-1').css('top', CSS.stick1.top);

            CSS.stick2.top += CONSTS.stick2Speed;
            $('#stick-2').css('top', CSS.stick2.top);

            CSS.ball.left += CONSTS.ballLeftSpeed; 
            CSS.ball.top += CONSTS.ballTopSpeed;      // ball yıkarıya  hareket
            
                // TOPUN yukarıya ve aşağı çarpması
            if (CSS.ball.top <= 0 ||
                CSS.ball.top >= CSS.arena.height - CSS.ball.height) {
                CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
            }
            // sol stick1 yukarıya aşağıya çarpma
            if (CSS.stick1.top <= 0 ||
                CSS.stick1.top >= CSS.arena.height - CSS.stick1.height) {
                CSS.stick1.top = CSS.stick1.top * -1;    // top alt veya üst duvarla çarpıştığında - y yönüne çevirir. 
            }

            // sağ stick2 yukarıya aşağıya çarpma
            if (CSS.stick2.top <= 0 ||
                CSS.stick2.top >= CSS.arena.height - CSS.stick2.height) {
                CSS.stick2.top = CSS.stick2.top * -1;
            }

            $('#pong-ball').css({top: CSS.ball.top , left: CSS.ball.left});

            if (CSS.ball.left <= CSS.stick.width) {
            	  CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1 ) || (puanArttır(2));
            }

            if (CSS.ball.left > CSS.arena.width - CSS.stick.width - CSS.ball.width ) {
            	  CSS.ball.top > CSS.stick2.top && CSS.ball.top < CSS.stick2.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1 )||  (puanArttır(1)) ;
            }
             
        }, CONSTS.gameSpeed);
    }

    function roll() {
        CSS.ball.top = 200;						
        CSS.ball.left = 450;
    
        var side = -1;
        if (Math.random() < 0.5) {
            side = 1;
        }
        CONSTS.ballTopSpeed = Math.random() * -2 -3;                //topun yukarı atış başlama çıkış hızı dikey yönlü 
        CONSTS.ballLeftSpeed = side * (Math.random() * 2 + 3);     // topun düşey yönlü 
    }

    function puanArttır(sayi){
      if(sayi ===1 ){
       CONSTS.puan1 +=1 ;
       $('#pn1').text(CONSTS.puan1);

       if( CONSTS.puan1 === 5){
           CSS.ball.top = 200;						
           CSS.ball.left = 450;
           stop = false;
           $('#pong-ball').css({top: CSS.ball.top , left: CSS.ball.left});
         }
    }
    
       if(sayi === 2 ){
           CONSTS.puan2 +=1 ;
       $('#pn2').text(CONSTS.puan2);
        if( CONSTS.puan2 === 5){
           CSS.ball.top = 300;						
           CSS.ball.left = 443;
           stop = false; 
           $('#pong-ball').css({top: CSS.ball.top , left: CSS.ball.left});
          }
      }
      roll();
    }       
    start();
}) ();